# Pqsym

Collaboration space for writing a joint article on post-quantum symmetric cryptography.

The goal for this project is to write a poster, then an article, on post-quantum symmetric cryptography (also called quantum-safe) and its latest advances, notably in the field of standardisation.

In short, in most of the world, post-quantum symmetric cryptography means replacing AES with a stronger construction, either based on it so as to make a security-compatibility tradeoff, or a totally different one, for maximum security, without however reaching the level of burden that generalising one-time pads (providing information-theoretically secure encryption through the use of one-time padding keys of the same length as the content to encipher) would imply.